using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit4.CollectionsLib;

namespace Bank
{
    class Branch
    {
        static private int counter = 0;

        private int NumBranch;
        private Node<Account> accounts;

        public Branch()
        {
            NumBranch = ++counter;
            this.accounts = null;
        }
        public void addAccount(Account a)
        {
            if (this.accounts == null)
                this.accounts = new Node<Account>(a);
            else
                this.accounts.SetNext(new Node<Account>(a));
        }

        // Gets
        public int getNumBranch()
        {
            return this.NumBranch;
        }
        public Node<Account> getAccounts()
        {
            return this.accounts;
        }
        
        // Sets
        public void setNumBranch(int numBranch)
        {
            this.NumBranch = numBranch;
        }
        public void setCustom(Node<Account> custom)
        {
            this.accounts = custom;
        }

        public bool accountExist(long accountNum)
        {
            Node<Account> temp = this.accounts;

            while (temp != null && temp.GetValue().GetAccountNum() != accountNum)
                temp = temp.GetNext();

            return temp != null;
        }

        public void allAccountsNums()
        {
            Node<Account> temp = this.accounts;
            int count = 1;
            while(temp != null)
            {
                Console.WriteLine("Customer " + count.ToString() + ": " + temp.GetValue().GetAccountNum());
                temp = temp.GetNext();
            }
        }

        public override string ToString()
        {
            string result = "Branch Num: " + this.NumBranch + "\n";
            Node<Account> temp = this.accounts;

            if (temp == null)
                result += "    No Accounts\n\n";
            while (temp != null)
            {
                result += "    Account No:  " + temp.GetValue().GetAccountNum() + "\n    Owner Name:  " + temp.GetValue().GetAccountName().GetName() + "\n    Balance:  " + temp.GetValue().GetBalance() + "\n\n";
                temp = temp.GetNext();
            }
            return result;
        }

        public bool closing(long accountNum)
        {
            if (this.accountExist(accountNum))
            {
                Node<Account> temp = this.accounts;
                while (temp != null && temp.GetValue().GetAccountNum() != accountNum)
                    temp = temp.GetNext();
                if (temp == null)
                    this.accounts = this.accounts.GetNext();
                if (temp.GetNext() == null)
                    temp = null;
                else
                    temp.SetNext(temp.GetNext().GetNext());
                return true;
            }
            return false;
        }
    }
}
