using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit4.CollectionsLib;

namespace Bank
{
    class Customer
    {
        private string ID;
        private string Name;
        private string Address;
        public Customer(string id, string name, string address)
        {
            this.ID = id;
            this.Name = name;
            this.Address = address;
        }
        public string GetID()
        {
            return this.ID;
        }
        public string GetName()
        {
            return this.Name;
        }
        public string GetAddress()
        {
            return this.Address;
        }
        public void SetID(string id)
        {
            this.ID = id;
        }
        public void SetName(string Name)
        {
            this.Name = Name;
        }
        public void SetAddress(string address)
        {
            this.Address = address;
        }
    }
}
