using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit4.CollectionsLib;

namespace Bank
{
    class Bank
    {
        private Node<Branch> Branches;

        public Bank()
        {
            this.Branches = new Node<Branch>(new Branch());
        }
        public Node<Branch> getBranches()
        {
            return this.Branches;
        }
        public string toString()
        {
            string result = "";
            Node<Branch> bracnhes = this.Branches;
            while(bracnhes != null)
            {
                result += bracnhes.GetValue().ToString();
                bracnhes = bracnhes.GetNext();
            }
            return result;
        }
        public void addBranch(Branch b)
        {
            if (this.Branches == null)
                this.Branches = new Node<Branch>(b);
            else
                this.Branches.SetNext(new Node<Branch>(b));
        }

        public int branchProfitable()
        {
            Node<Branch> temp = this.Branches;
            Node<Account> accounts;
            double sum = -1, max = -99999;
            int position = -1;


            while (temp != null)
            {
                accounts = temp.GetValue().getAccounts();
                while(accounts != null)
                {
                    sum += accounts.GetValue().GetBalance();
                    accounts = accounts.GetNext();
                }
                if(sum > max)
                {
                    max = sum;
                    position = temp.GetValue().getNumBranch();
                }
                sum = -1;
                temp = temp.GetNext();
            }
            return position;
        }

        public void printProfitable()
        {
            Node<Branch> temp = this.Branches;
            while(temp != null)
            {
                Console.WriteLine(temp.ToString());
                temp = temp.GetNext();
            }
        }
    }
}
