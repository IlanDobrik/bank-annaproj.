using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit4.CollectionsLib;

namespace Bank
{
    class Account
    {
        private static int count = 1000;

        private long accountNum;
        private Customer accountName;
        private double balance;
        private double framework;


        public Account(Customer aName)
        {
            this.accountNum = count++;
            this.accountName = aName;
            this.balance = 0;
            this.framework = 5000;
        }
        // Gets
        public long GetAccountNum()
        {
            return this.accountNum;
        }
        public Customer GetAccountName()
        {
            return this.accountName;
        }
        public double GetBalance()
        {
            return this.balance;
        }
        public double GetFramework()
        {
            return this.framework;
        }
        // Sets
        public void SetAccountNum(long num)
        {
            this.accountNum = num;
        }
        public void SetAccountName(Customer c)
        {
            this.accountName = c;
        }
        public void SetBalance(double b)
        {
            this.balance = b;
        }
        public void SetFramework(double f)
        {
            this.framework = f;
        }
        //Funcs
        public void deposit(double amount)
        {
            this.balance += amount;
        }
        public bool withdrawl(double amount)
        {
            if (amount > this.framework)
                return false;
            this.balance -= amount;
            if (this.balance < 0)
                Console.WriteLine("Warning!\nAccount in minus!");
            return true;
        }
    }
}
