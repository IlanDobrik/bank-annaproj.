using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit4.CollectionsLib;

namespace Bank
{
    class Program
    {
        static int welcome()
        {
            Console.WriteLine("Welcome to bank 'Banka'!");
            Console.WriteLine("Choose one of the following options:");
            Console.WriteLine("1. Create Account");
            Console.WriteLine("2. Add Branch");
            Console.WriteLine("3. Withdral");
            Console.WriteLine("4. Deposit");
            Console.WriteLine("5. Print Bank Details");
            Console.WriteLine("6. Delete Account");
            Console.WriteLine("99. Exit");

            int choice = 0;
            try
            {
                choice = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Input.");
            }
            while ( !(choice >= 1 && choice <= 6) && choice != 99)
            {
                Console.WriteLine("Invalid option.");
                try
                {
                    choice = int.Parse(Console.ReadLine());
                }
                catch(Exception e)
                {
                    Console.WriteLine("Invalid Input.");
                }
            }
            return choice;
        }

        static void createAccount(Bank banka)
        {
            string id = "", name = "", address = "";
            int branch = 0;
            Console.Clear();

            try
            {
                Console.WriteLine("Enter ID: "); 
                id = Console.ReadLine();

                Console.WriteLine("Enter Name: ");
                name = Console.ReadLine();

                Console.WriteLine("Enter Address: ");
                address = Console.ReadLine();

                Console.WriteLine("Choose brnach number: \n");
                Console.WriteLine(banka.toString());
                branch = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Input.\n");
                return;
            }
            
            Node<Branch> branches = banka.getBranches();
            while (branches != null && branches.GetValue().getNumBranch() != branch)
                branches = branches.GetNext();
            if (branches != null)
            {
                branches.GetValue().addAccount(new Account(new Customer(id, name, address)));
                Console.WriteLine("Account added!\n");
            }
            else
            {
                Console.WriteLine("Branch not found...\n");
            }
        }
        static Branch createBranch()
        {
            Console.Clear();
            Console.WriteLine("Creating Branch...");
            System.Threading.Thread.Sleep(1000);
            Branch b = new Branch();
            Console.WriteLine("Branch No. " + b.getNumBranch() + " created\n");
            return b;
        }

        static void withdraw(Bank b)
        {
            long accountnum = 0;
            Console.Clear();
            Console.WriteLine("Enter account number: ");
            try
            {
                accountnum = long.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Input.\n");
                return;
            }
            

            Node<Branch> branches = b.getBranches();
            while (branches != null && !branches.GetValue().accountExist(accountnum))
                branches = branches.GetNext();
            if (branches == null)
                Console.WriteLine("Account not found\n");
            else
            {
                Node<Account> accounts = branches.GetValue().getAccounts();
                while (accounts.GetValue().GetAccountNum() != accountnum)
                    accounts = accounts.GetNext();

                Console.WriteLine("How much would you like to withdraw?");
                try
                {
                    if (accounts.GetValue().withdrawl(double.Parse(Console.ReadLine())))//WIP
                        Console.WriteLine("Done.\n");
                    else
                        Console.WriteLine("Failed. Too big of a withdrawl!\nFrameWork: " + accounts.GetValue().GetFramework().ToString() + "\n");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid Input.\n");
                    return;
                }
            }
        }
        static void deposit(Bank b)
        {
            long accountnum = 0;
            Console.Clear();
            Console.WriteLine("Enter account number: ");
            try
            {
                accountnum = long.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Input.\n");
                return;
            }

            Node<Branch> branches = b.getBranches();
            while (branches != null && !branches.GetValue().accountExist(accountnum))
                branches = branches.GetNext();
            if (branches == null)
                Console.WriteLine("Account not found\n");
            else
            {
                Node<Account> accounts = branches.GetValue().getAccounts();
                while (accounts.GetValue().GetAccountNum() != accountnum)
                    accounts = accounts.GetNext();

                Console.WriteLine("How much would you like to deposit?");
                try
                {
                    accounts.GetValue().deposit(double.Parse(Console.ReadLine()));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid Input.\n");
                    return;
                }
                
                Console.WriteLine("Done.\n");
            }
        }
        static void deleteAccount(Bank banka)
        {
            Console.Clear();
            Console.WriteLine(banka.toString());

            long accountnum = 0;
            Console.WriteLine("Enter Account Num: ");
            try
            {
                accountnum = long.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Input.\n");
                return;
            }
            
            Node<Branch> branches = banka.getBranches();
            bool deleted = false;
            while (branches != null && !deleted)
            {
                if (branches.GetValue().closing(accountnum))
                    deleted = true;
                branches = branches.GetNext();
            }
            if(deleted)
                Console.WriteLine("Account Deleted.\n");
            else
                Console.WriteLine("Account Not Found.\n");
            
        }
        static void Main(string[] args)
        {
            Bank banka = new Bank();
            banka.getBranches().GetValue().addAccount(new Account(new Customer("135857390", "Ilan, Tavor, Ofek, Yair", "Tichon Hadera")));
            banka.getBranches().GetValue().getAccounts().GetValue().SetBalance(2);

            int choice = welcome();

            while (choice != 99)
            {
                switch (choice)
                {
                    case 1:
                        createAccount(banka);
                        break;
                    case 2:
                        banka.addBranch(createBranch());
                        break;
                    case 3:
                        withdraw(banka);
                        break;
                    case 4:
                        deposit(banka);
                        break;
                    case 5:
                        Console.WriteLine(banka.toString());
                        Console.WriteLine("Most profitable Branch: No. " + banka.branchProfitable() + "\n");
                        break;
                    case 6:
                        deleteAccount(banka);
                        break;
                }
                choice = welcome();
            }   
        }
    }
}
